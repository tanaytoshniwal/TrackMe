import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CompletemonthlyPage } from './completemonthly';

@NgModule({
  declarations: [
    CompletemonthlyPage,
  ],
  imports: [
    IonicPageModule.forChild(CompletemonthlyPage),
  ],
})
export class CompletemonthlyPageModule {}
