import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CompleteweeklyPage } from './completeweekly';

@NgModule({
  declarations: [
    CompleteweeklyPage,
  ],
  imports: [
    IonicPageModule.forChild(CompleteweeklyPage),
  ],
})
export class CompleteweeklyPageModule {}
