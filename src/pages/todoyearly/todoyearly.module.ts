import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TodoyearlyPage } from './todoyearly';

@NgModule({
  declarations: [
    TodoyearlyPage,
  ],
  imports: [
    IonicPageModule.forChild(TodoyearlyPage),
  ],
})
export class TodoyearlyPageModule {}
