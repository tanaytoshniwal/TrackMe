import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CompletedailyPage } from './completedaily';

@NgModule({
  declarations: [
    CompletedailyPage,
  ],
  imports: [
    IonicPageModule.forChild(CompletedailyPage),
  ],
})
export class CompletedailyPageModule {}
