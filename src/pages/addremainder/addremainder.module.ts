import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddremainderPage } from './addremainder';

@NgModule({
  declarations: [
    AddremainderPage,
  ],
  imports: [
    IonicPageModule.forChild(AddremainderPage),
  ],
})
export class AddremainderPageModule {}
