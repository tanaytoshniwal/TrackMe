import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CompleteyearlyPage } from './completeyearly';

@NgModule({
  declarations: [
    CompleteyearlyPage,
  ],
  imports: [
    IonicPageModule.forChild(CompleteyearlyPage),
  ],
})
export class CompleteyearlyPageModule {}
