# Track Me
A Hybrid mobile Application with a lots of features that helps one to stay consistent and track their progress in an efficient way.

## Features:
+   __Todo List__ : A simple todo list categorized specially for daily, weekly, monthly and yearly tasks
+   __Transactions Statistics__ : Track your daily transactions
+   __Notes__ : Keep your personal notes safe
+   __Remainders__ : Add remainders and never forget something important.

## Technology Stack:
+   Ionic 3
+   Firebase Authentication
+   Firestore Database

## Screenshots:
<p align="center">

<img src='https://github.com/tanaytoshniwal/TrackMe/blob/master/screenshots/1.png' width='150px'>
<img src='https://github.com/tanaytoshniwal/TrackMe/blob/master/screenshots/2.png' width='150px'>
<img src='https://github.com/tanaytoshniwal/TrackMe/blob/master/screenshots/3.png' width='150px'>
<img src='https://github.com/tanaytoshniwal/TrackMe/blob/master/screenshots/4.png' width='150px'>
<img src='https://github.com/tanaytoshniwal/TrackMe/blob/master/screenshots/5.png' width='150px'>
<img src='https://github.com/tanaytoshniwal/TrackMe/blob/master/screenshots/6.png' width='150px'>
<img src='https://github.com/tanaytoshniwal/TrackMe/blob/master/screenshots/7.png' width='150px'>
<img src='https://github.com/tanaytoshniwal/TrackMe/blob/master/screenshots/8.png' width='150px'>
<img src='https://github.com/tanaytoshniwal/TrackMe/blob/master/screenshots/9.png' width='150px'>
<img src='https://github.com/tanaytoshniwal/TrackMe/blob/master/screenshots/10.png' width='150px'>
<img src='https://github.com/tanaytoshniwal/TrackMe/blob/master/screenshots/11.png' width='150px'>

</p>